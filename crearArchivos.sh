#!/bin/bash
mkdir Archivos
cd Archivos
for(( i=1;i<=500;i++))
do
	mkdir $i
	let resul=$i%2
	if [ $resul -eq 0 ];
	then
		touch $i/Perrito
		chmod 652 $i/Perrito
		touch $i/Jirafa
		chmod u-w $i/Jirafa
		chmod u+x $i/Jirafa
		chmod g-r $i/Jirafa
		chmod g+w $i/Jirafa
		chmod g-x $i/Jirafa
		chmod o+wx $i/Jirafa
	elif [ $resul -ne 0 ];
	then
		touch $i/Gatico
		chmod u-r $i/Gatico
		chmod u-w $i/Gatico
		chmod g+wx $i/Gatico
		chmod o-r $i/Gatico
		touch $i/Lorito
		chmod 007 $i/Lorito
	fi
	let resul=$i%5
	if [ $resul -eq 0 ];
	then
		touch $i/Elefantico
		chmod 000 $i/Elefantico
	fi	
done
